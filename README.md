Learning the alarm component in the Dezyne Alarm example project.

Current repository contains a java 8 eclipse project to learn the Dezyne alarm
component using a learning purpose with the LearnLib tool.

In ./report/report.pdf is a report describes a set of learning experiments done 
on the Dezyne Alarm example project.

The correct Dezyne alarm example model project is in DezyneAlarmExampleProject/.

The java 8  source  generated from this model is in src/sul/alarmsystem/
together with the Dezyne runtime for java 8 generated models in header.java.
  
I wrote a SUL wrapper in src/sul/alarmsystem/AlarmSUL.java around the Alarm
component in src/sul/alarmsystem/Alarm.java. The AlarmSul class 
uses a learning purpose implemented in the files in the same folder:
 ISensorLearningPurpose.java ISirenLearningPurpose.java LearningPurpose.java

The main class the project is src/learner/Main.java. In this main class
you can setup which SUL you want to learn with which learner and testing method.
It is currently setup to learn the "AlarmSUL" SUL using the TTT learning
algorithm using random testing. 

The AlarmSUL can be configured in different modes by giving different
constructor parameters to it in src/learner/Main.java :

* learning SUL model without learning purpose
   
     USE_LEARNING_PURPOSE = false;
     CONSOLE_OUTPUTS_ONLY = false;
  
* learning SUL model  with learning purpose
   
     USE_LEARNING_PURPOSE = true;
     CONSOLE_OUTPUTS_ONLY = false;

* learning SUL INTERFACE  with learning purpose
   
     USE_LEARNING_PURPOSE = true;
     CONSOLE_OUTPUTS_ONLY = true;
