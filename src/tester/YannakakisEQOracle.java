package tester;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Scanner;

import org.apache.log4j.Logger;

import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.MembershipOracle;
import de.learnlib.oracles.DefaultQuery;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.util.graphs.dot.GraphDOT;
import net.automatalib.words.Word;
import net.automatalib.words.WordBuilder;

/**
 * Implements the Lee & Yannakakis suffixes by invoking an external program. Because of this indirection to an external
 * program, the findCounterexample method might throw a RuntimeException. Sorry for the hard-coded path to the
 * executable!
 *
 *
 * @param <O> is the output alphabet. (So a query will have type Word<String, Word<O>>.)
 *   => D is  <D> output domain type  => list of Strings in our case
 *   => no O is type of outputs of automaton!!
 */
public class YannakakisEQOracle<O> implements EquivalenceOracle.MealyEquivalenceOracle<String, O> {
	private static final Logger logger = Logger.getLogger(YannakakisEQOracle.class);
	private final MembershipOracle<String, Word<O>> sulMembershipOracle;
	
	
//	private final String yannakakisBinDir = "/Users/harcok/doc/workspaceMars/Yannakakis/build/Debug";	
    private final String yannakakisBinDir = "/usr/local/yannakakis/bin/";	
	private final String yannakakisCmd = "main";
	
	public enum Mode { 
		all,
		fixed,
		random
	}

	private  ProcessBuilder pb;
	
	

	// We buffer queries, in order to allow for parallel membership queries.
	private int bufferSize = 100;
	private ArrayList<DefaultQuery<String, Word<O>>> buffer = new ArrayList<>(bufferSize);

	private Process process;
	private Writer processInput;
	private BufferedReader processOutput;
	private RedirectToConsoleStdError errorGobbler;
    
	private long bound; 

	/**
	 * @param sulOracle The membership oracle of the SUL, we need this to check the output on the test suite
	 * 
	 * @param alphabets  List of alphabets  ??? 
	 * 
	 * @throws IOException
	 */
	public YannakakisEQOracle( MembershipOracle<String, Word<O>> sulOracle,  Mode mode, int max_k,int rnd_length, int seed) throws IOException {
		this.sulMembershipOracle = sulOracle;
	
		bound = Long.MAX_VALUE;  // no bound 
		pb = new ProcessBuilder(yannakakisBinDir + "/" + yannakakisCmd , "--seed",  Integer.toString(seed) , "=", mode.toString() , Integer.toString(max_k), Integer.toString(rnd_length));
		logger.debug("run: " +  yannakakisBinDir + "/" + yannakakisCmd + " --seed " +  Integer.toString(seed) +  " = " + mode.toString()  + " " +  Integer.toString(max_k)  + " " +  Integer.toString(rnd_length) );
	}


	public YannakakisEQOracle( MembershipOracle<String, Word<O>> sulOracle,  Mode mode, int max_k,int rnd_length, int seed,long bound) throws IOException {
		this.sulMembershipOracle = sulOracle;
	
		this.bound = bound;  
		pb = new ProcessBuilder(yannakakisBinDir + "/" + yannakakisCmd , "--seed",  Integer.toString(seed) , "=", mode.toString() , Integer.toString(max_k), Integer.toString(rnd_length));
		logger.debug("run: " +  yannakakisBinDir + "/" + yannakakisCmd + " --seed " +  Integer.toString(seed) +  " = " + mode.toString()  + " " +  Integer.toString(max_k)  + " " +  Integer.toString(rnd_length) );
	}
	
	/**
	 * Uses an external program to find counterexamples. The hypothesis will be written to stdin. Then the external
	 * program might do some calculations and write its test suite to stdout. This is in turn read here and fed
	 * to the SUL. If a discrepancy occurs, an counterexample is returned. If the external program exits (with value
	 * 0), then no counterexample is found, and the hypothesis is correct.
	 *
	 * This method might throw a RuntimeException if the external program crashes (which it shouldn't of course), or if
	 * the communication went wrong (for whatever IO reason).
	 * 
	 *  find counter example 
	 * @param machine
	 * @param inputs
	 * @param bound
	 * @return
	*/
	public DefaultQuery<String, Word<O>> findCounterExample(MealyMachine<?, String, ?, O> machine, Collection<? extends String> inputs) {
		try {
			setupProcess();
		} catch (IOException e) {
			throw new RuntimeException("Unable to start the external program: " + e);
		}

		
		
		long queryCount = 0;
		try {
			// Write the hypothesis to stdin of the external program
			GraphDOT.write(machine, inputs, processInput);
			processInput.flush();

			// Read every line outputted on stdout.
			// We buffer the queries, so that a parallel membership query can be applied
			String line;
			while ((line = processOutput.readLine()) != null) {
				// Read every string of the line, this will be a symbol of the input sequence.
				WordBuilder<String> wb = new WordBuilder<>();
				// scanner splits strings on space
				Scanner s = new Scanner(line);
				while(s.hasNext()) {
					wb.add(s.next());
				}

				// Convert to a word and test on the SUL				
				// A word<Symbol> is an ordered sequence of symbols. Words are generally immutable, 
				// i.e., a single Word object will never change (unless symbol objects are modified, which is however highly discouraged).
				Word<String> test = wb.toWord();
				
				// construct query object and add to buffer
				//
				// A query is a container for tests a learning algorithms performs, 
				// containing the actual test and the corresponding result.
				DefaultQuery<String, Word<O>> query = new DefaultQuery<>(test);
				buffer.add(query);

				// Break if we did not fin one in time  => more queries then bound (max)  => nothing run yet!!
				++queryCount;
				if(queryCount > bound) {
					
					// run queries in buffer 
					DefaultQuery<String, Word<O>> r = checkForCeAndEmptyBuffer(machine);
					if(r != null){
						closeAll();
						return r;
					}
					
					logger.debug("Testbound is reached: queryCount("+queryCount+") > bound("+bound+")" );
					closeAll();
					return null;
				}

				// run tests from buffer
				//
				// If the buffer is filled, we can perform the checks (possibly in parallel)
				if(buffer.size() >= bufferSize || bound <= bufferSize){
					DefaultQuery<String, Word<O>> r = checkForCeAndEmptyBuffer(machine);
					if(r != null){
						closeAll();
						return r;
					}
				}
			}
		} catch (IOException e) {
			throw new RuntimeException("Unable to communicate with the external program: " + e);
		}

		// At this point, the external program closed its stream, so it should have exited.
		if(process.isAlive()){
			logger.error("No counterexample but process stream still active!");
			closeAll();
			throw new RuntimeException("No counterexample but process stream still active!");
		}

		// If the program exited with a non-zero value, something went wrong (for example a segfault)!
		int ret = process.exitValue();
		if(ret != 0){
			logger.error("Something went wrong with the process: return value = " + ret);
			closeAll();
			throw new RuntimeException("Something went wrong with the process: return value = " + ret);
		}

		// Here, the program exited normally, without counterexample, so we may return null.
		return null;
	}
	

	/** runs set of queries from buffer  (in parallel??)
	 *  
	 * @param machine
	 * @return query if found counter example , else null
	 */
	private DefaultQuery<String, Word<O>> checkForCeAndEmptyBuffer(MealyMachine<?, String, ?, O> machine){
		sulMembershipOracle.processQueries(buffer);
		DefaultQuery<String, Word<O>> r = checkForCounterExampleInBuffer(machine);
		buffer.clear();
		return r;
	}

	/**
	 * Check if one of queries in buffer is a counter example 
	 * 
	 * @param machine
	 * @return query if found counter example , else null
	 */
	private DefaultQuery<String, Word<O>> checkForCounterExampleInBuffer(MealyMachine<?, String, ?, O> machine){
		for(DefaultQuery<String, Word<O>> query : buffer){
			if ( isCounterExample(machine,query) )
				return query;
		}
		return null;
	}
	
    /**  check sut query is a counter example on given hypothesis
     * 
     * @param hypothesis
     * @param query : sut query : contains inputs and the corresponding outputs for the sut
     * @return
     */
	
	private <I,O> boolean isCounterExample(MealyMachine<?,I,?,O> hypothesis,DefaultQuery<I, net.automatalib.words.Word<O>> query ) {
		net.automatalib.words.Word<O> o1 = hypothesis.computeOutput(query.getInput());
		net.automatalib.words.Word<O> o2 = query.getOutput();

		assert o1 != null;
		assert o2 != null;

		// If equal => no counterexample :(
		if(!o1.equals(o2)) return true;	
		return false;
	}
	
	
	
	//========================================================================================================
	// helper code for setting up, running and finishing  yannakakis  c++ program
	//========================================================================================================
	

	/**
	 * A small class to print all stuff to stderr. Useful as I do not want stderr and stdout of the external program to
	 * be merged, but still want to redirect stderr to java's stderr.
	 */
	class RedirectToConsoleStdError extends Thread {
		private final InputStream stream;
		private final String prefix;

		RedirectToConsoleStdError(InputStream stream, String prefix) {
			this.stream = stream;
			this.prefix = prefix;
		}

		public void run() {
			try {
				BufferedReader reader = new BufferedReader(new InputStreamReader(stream));
				String line;
				while ((line = reader.readLine()) != null)
					logger.error(prefix + "> " + line);
			} catch (IOException e) {
				// It's fine if this thread crashes, nothing depends on it
				logger.error(e.getMessage());
				logger.error(e.getStackTrace().toString());
			}
		}
	}

	/**
	 * Starts the process and creates buffered/whatnot streams for stdin stderr or the external program
	 * @throws IOException if the process could not be started (see ProcessBuilder.start for details).
	 */
	private void setupProcess() throws IOException {
		process = pb.start();
		processInput = new OutputStreamWriter(process.getOutputStream());
		processOutput = new BufferedReader(new InputStreamReader(process.getInputStream()));
		
		// redirect stderr of process to error log!
		errorGobbler = new RedirectToConsoleStdError(process.getErrorStream(), "ERROR> main");
		errorGobbler.start();
	}

	/**
	 * I thought this might be a good idea, but I'm not a native Java speaker, so maybe it's not needed.
	 */
	private void closeAll() {
		// Since we're closing, I think it's ok to continue in case of an exception
		try {
			processInput.close();
			processOutput.close();
		} catch (IOException e) {
			logger.error(e.getMessage());
			logger.error(e.getStackTrace().toString());
		}
		try {
			errorGobbler.join(10);
		} catch (InterruptedException e) {
			logger.error(e.getMessage());
			logger.error(e.getStackTrace().toString());
		}
		process.destroy();
		process = null;
		processInput = null;
		processOutput = null;
		errorGobbler = null;
	}
}
