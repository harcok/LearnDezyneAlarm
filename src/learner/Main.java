package learner;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.zeroturnaround.exec.InvalidExitValueException;
import org.zeroturnaround.exec.ProcessExecutor;

import com.google.common.collect.ImmutableSet;
import com.google.common.collect.Lists;

import de.learnlib.acex.analyzers.AcexAnalyzers;
import de.learnlib.algorithms.kv.mealy.KearnsVaziraniMealy;
import de.learnlib.algorithms.lstargeneric.ce.ObservationTableCEXHandlers;
import de.learnlib.algorithms.lstargeneric.closing.ClosingStrategies;
import de.learnlib.algorithms.lstargeneric.mealy.ExtensibleLStarMealy;
import de.learnlib.algorithms.ttt.mealy.TTTLearnerMealy;
import de.learnlib.api.EquivalenceOracle;
import de.learnlib.api.LearningAlgorithm;
import de.learnlib.api.MembershipOracle.MealyMembershipOracle;
import de.learnlib.api.SUL;
import de.learnlib.eqtests.basic.WMethodEQOracle;
import de.learnlib.eqtests.basic.WpMethodEQOracle;
import de.learnlib.eqtests.basic.mealy.RandomWalkEQOracle;
import de.learnlib.experiments.Experiment.MealyExperiment;
import de.learnlib.oracles.DefaultQuery;
import de.learnlib.oracles.ResetCounterSUL;
import de.learnlib.oracles.SULOracle;
import de.learnlib.oracles.SymbolCounterSUL;
import de.learnlib.statistics.Counter;
import net.automatalib.automata.transout.MealyMachine;
import net.automatalib.automata.transout.impl.compact.CompactMealy;
import net.automatalib.automata.transout.impl.compact.CompactMealyTransition;
import net.automatalib.commons.dotutil.DOT;
import net.automatalib.commons.util.IOUtil;
import net.automatalib.util.automata.builders.AutomatonBuilders;
import net.automatalib.util.automata.builders.MealyBuilder;
import net.automatalib.util.graphs.dot.GraphDOT;
import net.automatalib.words.Alphabet;
import net.automatalib.words.Word;
import net.automatalib.words.impl.Alphabets;
import net.automatalib.words.impl.EnumAlphabet;
import net.automatalib.words.impl.SimpleAlphabet;
import sul.ExampleSUL;
import sul.SocketSUL;
import sul.alarmsystem.AlarmSUL;
import tester.YannakakisEQOracle;


import de.learnlib.simulator.sul.MealySimulatorSUL;


import   learner.Main.Input;
import static learner.Main.Input.BUTTON;
import static learner.Main.Input.CLEAN;
import static  learner.Main.Input.POD;
import static  learner.Main.Input.WATER;

/**
 * General learning testing framework. The most important parameters are the input alphabet and the SUL (The
 * first two static attributes). Other settings can also be configured.
 * 
 * Based on the learner experiment setup of Joshua Moerman, https://gitlab.science.ru.nl/moerman/Learnlib-Experiments
 * 
 * @author Ramon Janssen
 * 
 * further enhanced by Harco Kuppens
 */
public class Main {
	private static final Logger logger = Logger.getLogger(Main.class);
	
	//*****************//
 	// logging         //
	//*****************//	
	

	public enum LOGType { simple, standard, advanced }
	 // "log4j.simple.conf"     : no logfile, no special logging of traces,  layout of logline is just message
	 // "log4j.standard.conf"   : logfile, but no special logging of traces,  layout of logline is just level, function, message
	 // "log4j.advanced.conf"   : logfile, and special logging of traces,  layout of logline is detailistic
	private static final LOGType logType = LOGType.advanced;
	

	
	//*****************//
 	// graphviz        //
	//*****************//
	
	private static final boolean GENERATE_PDF_FROM_DOT = true;
	private static final long DOT_CMD_TIMEOUT_IN_SECONDS = 10;

	
	//*****************//
 	// SUL information //
	//*****************//
	// Defines the input alphabet (you can even use other types than string, if you 
	// change the generic-values, e.g. make your SUL of type SUL<Integer, Float> for int-input and float-output
	private static Alphabet<String> inputAlphabet;
	// There are two SULs predefined, an example (see ExampleSul.java) and a socket SUL which connects to the SUL over socket
	
	
	
	// suls available 
	public enum SULType { Example, CoffeeFluentBuild, Socket, Alarm, CoffeeModelBuild }
	
	private static final SULType sulType = SULType.Alarm;
	//private static final SULType sulType = SULType.CoffeeFluentBuild;
	//private static final SULType sulType = SULType.CoffeeModelBuild;
	//private static final SULType sulType = SULType.Example;  //basic  example
	
	// For SULs over socket, the socket address/port can be set here
	private static final InetAddress socketIp = InetAddress.getLoopbackAddress();
	private static final int socketPort = 7890;
	private static final boolean printNewLineAfterEveryInput = true; // print newlines in the socket connection
	private static final String resetCmd = "reset"; // the command to send over socket to reset sut
	
	
	//*******************//
 	// Learning settings //
	//*******************//
	// file for writing the resulting .dot-file and .pdf-file (extensions are added automatically)
	private static final String FINAL_MODEL_FILENAME = "learnedModel",
								INTERMEDIATE_HYPOTHESIS_FILENAME = "log/hypothesis";

	// the learning algorithms. LStar is the basic algorithm, TTT performs much faster
	// but is a bit more inaccurate and produces more intermediate hypotheses, so test well)
	public enum LearningMethod { LStar, RivestSchapire, TTT, KearnsVazirani }
	private static  LearningMethod learningAlgorithm = LearningMethod.TTT; // default
	
	
	//*******************//
 	// Testing settings  //
	//*******************//
	
	// the testing algorithms 
	public enum TestingMethod { RandomWalk, WMethod, WpMethod, LeeYannakakis }
	// Random walk is the simplest, but performs badly on large models: the chance of hitting a
	// erroneous long trace is very small
	private static  TestingMethod testMethod = TestingMethod.RandomWalk; //default

	
	// for random walk, the chance to do a reset after an input and the number of
	// inputs to test before accepting a hypothesis
	
	// for alarm system :
	private static final double chanceOfResetting = 0;  // 0.1 means 10 procent!
	private static final int numberOfSymbols =  100; //1000; //100;
	
//	// for coffee machine :   
//	private static final double chanceOfResetting = 0.1;  // 0.1 means 10 procent!
//	private static final int numberOfSymbols =  10000; //1000; //100;

	
	//*******************//
 	// Experiment as whole settings  //
	//*******************//

	// Simple experiments produce very little feedback, controlled produces feedback after
	// every hypotheses and are better suited to adjust by programming
	private static final boolean runControlledExperiment = true;
	// For controlled experiments only: store every hypotheses as a file. Useful for 'debugging'
	// if the learner does not terminate (hint: the TTT-algorithm produces many hypotheses).
	private static final boolean saveAllHypotheses = true;


	
	/**/
	public static enum Input {
		WATER,
		POD,
		BUTTON,
		CLEAN
	}
	
	
	public final static String WATER = "WATER";
	public final static String POD = "POD";
	public final static String BUTTON = "BUTTON";
	public final static String CLEAN = "CLEAN";
	
	public final static String out_ok = "ok";
	public final static String out_error = "error";
	public final static String out_coffee = "coffee!";



	

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void main(String [] args) throws IOException {
		
	 	// logging settings //
		recreateDir("log/");
		String loggingConfigFile = "log4j." + logType + ".conf"; //  (log4j.simple.conf, log4j.standard.conf, log4j.advanced.conf  )
		PropertyConfigurator.configure("config/" + loggingConfigFile);

		// Load the actual SUL-class, depending on which SUL-type is set at the top of this file
		// You can also program an own SUL-class if you extend SUL<String,String> (or SUL<S,T> in
		// general, with S and T the input and output types - you'll have to change some of the
		// code below)
		SUL<String,String> sul;
		switch (sulType) {
		case Example: 
			sul = new ExampleSUL();
			inputAlphabet = new SimpleAlphabet<String>(ImmutableSet.of("a", "b", "c"));	
			learningAlgorithm = LearningMethod.TTT;
			testMethod = TestingMethod.RandomWalk;
			//testMethod = TestingMethod.LeeYannakakis;
			break;
		case CoffeeFluentBuild: 
			learningAlgorithm = LearningMethod.TTT;
			testMethod = TestingMethod.RandomWalk;
			
			//Alphabet<Input> alphabet = Alphabets.fromEnum(Input.class);

			//MealyBuilder<Integer,Input,CompactMealyTransition<String>,String,CompactMealy<Input,String>>  mealyBuilder = new MealyBuilder<>( new CompactMealy<Input,String>(alphabet) );
			// CompactMealy<Input, String> machine=mealyBuilder.withInitial("a").create();
			//CompactMealy<Input, String> machine = AutomatonBuilders.<Input, String> newMealy(alphabet).withInitial("a").create();		
			
			
			//MealyBuilder<Integer,Input,CompactMealyTransition<String>,String,CompactMealy<Input,String>>  mealyBuilder = new MealyBuilder<>( new CompactMealy<Input,String>(alphabet) );
			// CompactMealy<Input, String> machine=mealyBuilder.withInitial("a").create();
			//CompactMealy<Input, String> machine = AutomatonBuilders.<Input, String> newMealy(alphabet).withInitial("a").create();		
			
			Alphabet<String> alphabet = Alphabets.fromArray(WATER,POD,BUTTON,CLEAN);
			inputAlphabet = alphabet;

					CompactMealy<String, String> fluentCoffeeMachine = AutomatonBuilders.<String, String> newMealy(alphabet)		
			        .withInitial("a")
	    			.from("a")
	    				.on(WATER).withOutput(out_ok).to("c")
	    				.on(POD).withOutput(out_ok).to("b")
	    				.on(BUTTON).withOutput(out_error).to("f")
	    				.on(CLEAN).withOutput(out_ok).loop()
	    			.from("b")
	    				.on(WATER).withOutput(out_ok).to("d")
	    				.on(POD).withOutput(out_ok).loop()
	    				.on(BUTTON).withOutput(out_error).to("f")
	    				.on(CLEAN).withOutput(out_ok).to("a")
	    			.from("c")
	    				.on(WATER).withOutput(out_ok).loop()
	    				.on(POD).withOutput(out_ok).to("d")
	    				.on(BUTTON).withOutput(out_error).to("f")
	    				.on(CLEAN).withOutput(out_ok).to("a")
	    			.from("d")
	    				.on(WATER, POD).withOutput(out_ok).loop()
	    				.on(BUTTON).withOutput(out_coffee).to("e")
	    				.on(CLEAN).withOutput(out_ok).to("a")
	    			.from("e")
	    				.on(WATER, POD, BUTTON).withOutput(out_error).to("f")
	    				.on(CLEAN).withOutput(out_ok).to("a")
	    			.from("f")
	    				.on(WATER, POD, BUTTON, CLEAN).withOutput(out_error).loop()
	    		.create();
					

					
					//MealySimulatorSUL<String,String> simsul  = new MealySimulatorSUL(machine);
					sul= new MealySimulatorSUL(fluentCoffeeMachine);;
					
			break;
		case CoffeeModelBuild:
			learningAlgorithm = LearningMethod.TTT;
			testMethod = TestingMethod.RandomWalk;
			
			inputAlphabet = Alphabets.fromArray(WATER,POD,BUTTON,CLEAN);

			CompactMealy<String, String> machine = new CompactMealy<String,String>(inputAlphabet);
			
			Integer a = machine.addInitialState(), b = machine.addState(), c = machine
			.addState(), d = machine.addState(), e = machine.addState(), f = machine
			.addState();

			machine.addTransition(a, WATER, c, out_ok);
			machine.addTransition(a, POD, b, out_ok);
			machine.addTransition(a, BUTTON, f, out_error);
			machine.addTransition(a, CLEAN, a, out_ok);
		
			machine.addTransition(b, WATER, d, out_ok);
			machine.addTransition(b, POD, b, out_ok);
			machine.addTransition(b, BUTTON, f, out_error);
			machine.addTransition(b, CLEAN, a, out_ok);
		
			machine.addTransition(c, WATER, c, out_ok);
			machine.addTransition(c, POD, d, out_ok);
			machine.addTransition(c, BUTTON, f, out_error);
			machine.addTransition(c, CLEAN, a, out_ok);
		
			machine.addTransition(d, WATER, d, out_ok);
			machine.addTransition(d, POD, d, out_ok);
			machine.addTransition(d, BUTTON, e, out_coffee);
			machine.addTransition(d, CLEAN, a, out_ok);
		
			machine.addTransition(e, WATER, f, out_error);
			machine.addTransition(e, POD, f, out_error);
			machine.addTransition(e, BUTTON, f, out_error);
			machine.addTransition(e, CLEAN, a, out_ok);
		
			machine.addTransition(f, WATER, f, out_error);
			machine.addTransition(f, POD, f, out_error);
			machine.addTransition(f, BUTTON, f, out_error);
			machine.addTransition(f, CLEAN, f, out_error);
			
			sul= new MealySimulatorSUL(machine);;
		
			break;
		case Alarm: 

		    final boolean USE_LEARNING_PURPOSE = true;
		    final boolean CONSOLE_OUTPUTS_ONLY = false;
		 	sul = new AlarmSUL(USE_LEARNING_PURPOSE,CONSOLE_OUTPUTS_ONLY);
		
		 	inputAlphabet = new SimpleAlphabet<String>(ImmutableSet.of("console:arm","console:disarm","sensor:disabled","sensor:triggered"));	
			
		 	learningAlgorithm = LearningMethod.TTT;
			testMethod = TestingMethod.RandomWalk;
			
			break;
		case Socket:
			// for SUL the learner communicates with over a network socket
			sul = new SocketSUL(socketIp, socketPort, printNewLineAfterEveryInput, resetCmd);
			break;
		default:
			throw new RuntimeException("No SUL-type defined");
		}
		
		
		// Wrap the SUL in counters for symbols/resets, so that we can record some statistics
		SymbolCounterSUL<String, String> symbolCounterSul = new SymbolCounterSUL<>("symbol counter", sul);
		ResetCounterSUL<String, String> resetCounterSul = new ResetCounterSUL<>("reset counter", symbolCounterSul);
		Counter nrSymbols = symbolCounterSul.getStatisticalData(), nrResets = resetCounterSul.getStatisticalData();
		// we should use the sul only through those wrappers
		sul = resetCounterSul;
		
		
		
		// log traces during learning
		SUL<String,String> learning_sul = new LogTraces("memTraces",sul);
		MealyMembershipOracle<String,String> learning_sulOracle = new SULOracle<>(learning_sul);
		

		// Choosing a learner
		LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> learner = null;
		switch (learningAlgorithm){
			case LStar:
				learner = new ExtensibleLStarMealy<>(inputAlphabet, learning_sulOracle, Lists.<Word<String>>newArrayList(), ObservationTableCEXHandlers.CLASSIC_LSTAR, ClosingStrategies.CLOSE_SHORTEST);
				break;
			case RivestSchapire:
				learner = new ExtensibleLStarMealy<>(inputAlphabet, learning_sulOracle, Lists.<Word<String>>newArrayList(), ObservationTableCEXHandlers.RIVEST_SCHAPIRE, ClosingStrategies.CLOSE_SHORTEST);
				break;
			case TTT:
				learner = new TTTLearnerMealy<>(inputAlphabet, learning_sulOracle, AcexAnalyzers.LINEAR_FWD);
				break;
			case KearnsVazirani:
				learner = new KearnsVaziraniMealy<>(inputAlphabet, learning_sulOracle, false, AcexAnalyzers.LINEAR_FWD);
				break;
			default:
				throw new RuntimeException("No learner selected");
		}
		
		// log traces during learning
		SUL<String,String> testing_sul = new LogTraces("equivTraces",sul);
		MealyMembershipOracle<String,String> testing_sulOracle = new SULOracle<>(testing_sul);
		
		// Choosing an equivalence oracle
		EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracle = null;
		EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracleFixed = null;
		switch (testMethod){
			// simplest method, but doesn't perform well in practice, especially for large models
			case RandomWalk:
				eqOracle = new RandomWalkEQOracle<>(chanceOfResetting, numberOfSymbols, true, new Random(123456l), testing_sul);
				break;
			// Other methods are somewhat smarter than random testing: state coverage, trying to distinguish states, etc.
			case WMethod:
				eqOracle = new WMethodEQOracle.MealyWMethodEQOracle<>(3, testing_sulOracle);
				break;
			case WpMethod:
				eqOracle = new WpMethodEQOracle.MealyWpMethodEQOracle<>(3, testing_sulOracle);
				break;
			case LeeYannakakis:			
				eqOracle = new YannakakisEQOracle<>(testing_sulOracle,YannakakisEQOracle.Mode.random, 8, 8,7,200);
				eqOracleFixed = new YannakakisEQOracle<>(testing_sulOracle,YannakakisEQOracle.Mode.fixed, 2, 2,7);
                break;
			default:
				throw new RuntimeException("No test oracle selected!");
		}
		
		// Running the actual experiments!
		if (runControlledExperiment) {
			runControlledExperiment(learner, eqOracle, eqOracleFixed, nrSymbols, nrResets, inputAlphabet);
		} else {
			runSimpleExperiment(learner, eqOracle, inputAlphabet);
		}
	}
	
	/**
	 * Simple example of running a learning experiment
	 * @param learner Learning algorithm, wrapping the SUL
	 * @param eqOracle Testing algorithm, wrapping the SUL
	 * @param alphabet Input alphabet
	 * @throws IOException if the result cannot be written
	 */
	public static void runSimpleExperiment(
			LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> learner,
			EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracle,
			Alphabet<String> alphabet) throws IOException {
		MealyExperiment<String, String> experiment = new MealyExperiment<String, String>(learner, eqOracle, alphabet);
		experiment.run();
		logger.info("Ran " + experiment.getRounds().getCount() + " rounds");
		produceOutput(FINAL_MODEL_FILENAME, experiment.getFinalHypothesis(), alphabet);
	}
	
	/**
	 * More detailed example of running a learning experiment. Starts learning, and then loops testing,
	 * and if counterexamples are found, refining again. Also prints some statistics about the experiment
	 * @param learner learner Learning algorithm, wrapping the SUL
	 * @param eqOracle Testing algorithm, wrapping the SUL
	 * @param nrSymbols A counter for the number of symbols that have been sent to the SUL (for statistics)
	 * @param nrResets A counter for the number of resets that have been sent to the SUL (for statistics)
	 * @param alphabet Input alphabet
	 * @throws IOException
	 */
	public static void runControlledExperiment(
			LearningAlgorithm<MealyMachine<?, String, ?, String>, String, Word<String>> learner,
			EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracle,
			EquivalenceOracle<MealyMachine<?, String, ?, String>, String, Word<String>> eqOracleFixed,
			Counter nrSymbols, Counter nrResets,
			Alphabet<String> alphabet) throws IOException {
		
		try {
			// prepare some counters for printing statistics
			int stage = 0;
			long lastNrResetsValue = 0, lastNrSymbolsValue = 0;
			
			// start the actual learning
			logger.info("===========================================================================");
			logger.info("START LEARNING PROCESS");
			logger.info("===========================================================================");
			logger.info("BEGIN LEARNING");
			learner.startLearning();
			
			while(true) {
				
				logger.info("---------------------------------------------------------------------------");
				logger.info("FINISHED LEARNING");

				
				// store hypothesis as file
				if(saveAllHypotheses) {
					String outputFilename =  INTERMEDIATE_HYPOTHESIS_FILENAME + stage;
					produceOutput(outputFilename, learner.getHypothesisModel(), alphabet);
					logger.info("model size" + learner.getHypothesisModel().getStates().size());
				}
	
				// Print statistics
				
				logger.info(stage + ": " + Calendar.getInstance().getTime());
				// Log number of queries/symbols
				logger.info("Hypothesis size: " + learner.getHypothesisModel().size() + " states");
				long roundResets = nrResets.getCount() - lastNrResetsValue, roundSymbols = nrSymbols.getCount() - lastNrSymbolsValue;
				logger.info("learning queries/symbols: " + nrResets.getCount() + "/" + nrSymbols.getCount()
						+ "(" + roundResets + "/" + roundSymbols + " this learning round)");
				
				logger.info("---------------------------------------------------------------------------");
				logger.info("START TESTING");
				
				
				
				lastNrResetsValue = nrResets.getCount();
				lastNrSymbolsValue = nrSymbols.getCount();
				

				
				// Search for CE
				DefaultQuery<String, Word<String>> ce = eqOracle.findCounterExample(learner.getHypothesisModel(), alphabet);
				
				
				
				// retry with second eqOracle (eqOracleFixed)
				if ( ce == null && eqOracleFixed != null) {
					
					// Log number of queries/symbols
					roundResets = nrResets.getCount() - lastNrResetsValue;
					roundSymbols = nrSymbols.getCount() - lastNrSymbolsValue;

					logger.info("testing queries/symbols: " + nrResets.getCount() + "/" + nrSymbols.getCount()
					+ "(" + roundResets + "/" + roundSymbols + " this testing round)");

					
					logger.info("---------------------------------------------------------------------------");
					logger.info("FINISHED RANDOM TESTING  => START FIXED TESTING");
					logger.info("---------------------------------------------------------------------------");
					
					
					lastNrResetsValue = nrResets.getCount();
					lastNrSymbolsValue = nrSymbols.getCount();

					ce = eqOracleFixed.findCounterExample(learner.getHypothesisModel(), alphabet);
					
				} 
				
				logger.info("counterexample on sut: " + ce);
				
				// Log number of queries/symbols
				roundResets = nrResets.getCount() - lastNrResetsValue;
				roundSymbols = nrSymbols.getCount() - lastNrSymbolsValue;
				
				logger.info("---------------------------------------------------------------------------");
				logger.info("FINISHED TESTING");

				logger.info("testing queries/symbols: " + nrResets.getCount() + "/" + nrSymbols.getCount()
						+ "(" + roundResets + "/" + roundSymbols + " this testing round)");
				
				logger.info("===========================================================================");
				
				
				lastNrResetsValue = nrResets.getCount();
				lastNrSymbolsValue = nrSymbols.getCount();
				
				if(ce == null) {
					// No counterexample found, stop learning
					logger.info("FINISHED LEARNING PROCESS");
					logger.info("===========================================================================");
					
					produceOutput(FINAL_MODEL_FILENAME, learner.getHypothesisModel(), alphabet);					
					break;
				} else {
					// Counterexample found, rinse and repeat
					stage++;
					
					logger.info("RESTART LEARNING with ce: "  + ce);  // prefix: Empty word ; suffix : inputs; output: outputs
					learner.refineHypothesis(ce);
				}
			}
		} catch (Exception e) {
			String errorHypName = "hyp.before.crash.dot";
			produceOutput(errorHypName, learner.getHypothesisModel(), inputAlphabet);
			throw e;
		}
		
		
	}
	
	/**
	 * Produces a dot-file and a PDF (if graphviz is installed)
	 * @param fileName filename without extension - will be used for the .dot and .pdf
	 * @param model
	 * @param alphabet
	 * @param verboseError whether to print an error explaing that you need graphviz
	 * @throws FileNotFoundException
	 * @throws IOException
	 */
	public static void produceOutput(String fileName, MealyMachine<?,String,?,String> model, Alphabet<String> alphabet) throws FileNotFoundException, IOException {
		PrintWriter dotWriter = new PrintWriter(fileName + ".dot");
		GraphDOT.write(model, alphabet, dotWriter);
		dotWriter.close();
		
		if ( GENERATE_PDF_FROM_DOT ) convertDot2Format(fileName, "pdf" );
		
	}
	
	public static void convertDot2Format(String basename, String format ) {
		String dotfile = new File(basename + ".dot").getAbsolutePath();
		String outfile = new File(basename + "." + format).getAbsolutePath();
		
		// see https://github.com/zeroturnaround/zt-exec
		ProcessExecutor cmd=new ProcessExecutor()
				                  .command("dot", "-T" + format, dotfile, "-o", outfile)
				                  .exitValueNormal()
				                  .timeout(DOT_CMD_TIMEOUT_IN_SECONDS, TimeUnit.SECONDS);
		try {
			cmd.execute();
		} catch (InvalidExitValueException | IOException | InterruptedException | TimeoutException e) {
			// none fatal error, just report and continue			
			logger.error("problem in converting " +basename  + ".dot to " + format );
			logger.debug(e);
		}
	}
	
	

    /*
     *  remove directory with all its contents,
     *  where subdirectories are recursively removed
     */
    static public void rmdirhier(String dir)
    {

        File path = new File(dir);
        _rmdir(path);
    }

    static protected void _rmdir(File path)
    {
        if (path == null)
            return;
        if (path.exists())
        {
            for (File f : path.listFiles())
            {
                if (f.isDirectory())
                {
                    _rmdir(f);
                    f.delete();
                }
                else
                {
                    f.delete();
                }
            }
            path.delete();
        }
    }

    static public void mkdir(String dir) {
        new File(dir).mkdir();
    }
    
    static public void recreateDir(String dir) {
    	rmdirhier(dir);
    	mkdir(dir);
    }
    
}
