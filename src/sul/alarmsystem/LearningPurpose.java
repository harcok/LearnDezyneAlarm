package sul.alarmsystem;

public interface LearningPurpose {

	
	/**
	 *  keeping track of current state of interface by inputs to interface from component(s) interacting with interface
	 *  
	 * @param input
	 * 
	 *  returns 
	 *     boolean saying whether input is enabled or not (illegal)
	 */
	public boolean stepOnInput ( String input );
	
	
	/**
	 *  keeping track of current state of interface by tracking outputs from interface caused by internal actions  
	 *  
	 * @param output
	 * 
	 * returns
	 *     boolean saying whether output is possible or not
	 */
	public boolean stepOnOutput ( String output );
}
