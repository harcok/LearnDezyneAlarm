package sul.alarmsystem;



import java.util.LinkedList;

import org.apache.log4j.Logger;


import de.learnlib.api.SUL;
import de.learnlib.api.SULException;

/**
 * SUL wrapper for Alarm component in alarm example in Dezyne Verum 
 * 
 * @author Harco Kuppens
 */
public class AlarmSUL implements SUL<String, String> {
	private static final Logger logger = Logger.getLogger(AlarmSUL.class);
	
	private final boolean USE_LEARNING_PURPOSE;
	private final boolean CONSOLE_OUTPUTS_ONLY; 
	
	public  ISirenLearningPurpose sirenLearningPurpose;
	public  ISensorLearningPurpose sensorLearningPurpose;
	
	public static LinkedList<String> queue = new LinkedList<String>();
	public static Alarm alarm;

	
	private final String IF_IMPOSSIBLE_OUTPUT = "DISABLED_INPUT";  // impossible output for interface means that the input for the component is impossible => disabled by interfaces
	private final String IF_ILLEGAL_INPUT = "ILLEGAL_OUTPUT";	// illegal input for interface is illegal output for component 
	private final  String INTERNAL_OUTPUT = "INTERNAL";

	
	public AlarmSUL(boolean USE_LEARNING_PURPOSE, boolean CONSOLE_OUTPUTS_ONLY) {
		this.USE_LEARNING_PURPOSE=USE_LEARNING_PURPOSE;
		this.CONSOLE_OUTPUTS_ONLY= CONSOLE_OUTPUTS_ONLY;
	}

	public void reset() {
		Locator locator = new Locator();
		//Action illegal_handler = new Action() {public void action() {throw new RuntimeException("illegal");}};
		Action illegal_handler = new Action() {public void action() {
			logger.debug("    illegal");
			queue.add("console:illegal"); // only component running in runtime
		}};
		
		Runtime runtime = new Runtime(illegal_handler);
		
		locator.set(runtime);

		//construct the system
	    alarm = new Alarm(locator,"alarm");
	    
	    // construct learning purposes for below interfaces
	    sirenLearningPurpose = new ISirenLearningPurpose();
	    sensorLearningPurpose = new ISensorLearningPurpose();
		
	    
	    // collecting OUTPUTS from alarm component and process through learning purpose:
	    //------------------------------------------------------------------------------
	    //  - for interface above : output of interface
	    //  - for interface below : input of interface 
	    
	    // interface above
	    
	    alarm.console.out.deactivated = new Action() {public void action() {
			logger.debug("    console_deactivated");
			queue.add("console:deactivated");
		}}; 
		
		alarm.console.out.detected = new Action() {public void action() {
			logger.debug("    console_detected");
			queue.add("console:detected");
		}}; 
		
		// interface below
		
		alarm.sensor.in.enable =  new Action() {public void action() {
			logger.debug("    sensor_enable");
			String output = "sensor:enable";
			if ( USE_LEARNING_PURPOSE ) output = processOutputWithLearningPurpose(output,sensorLearningPurpose);
			queue.add(output);
			   
		}}; 
		
		alarm.sensor.in.disable =  new Action() {public void action() {
			logger.debug("    sensor_disable");
			String output = "sensor:disable";
			if ( USE_LEARNING_PURPOSE ) output = processOutputWithLearningPurpose(output,sensorLearningPurpose);
			queue.add(output);
		}};
		
	    alarm.siren.in.turnoff = new Action() {public void action() {
			logger.debug("    siren_turnoff");
			String output = "siren:turnoff";
			if ( USE_LEARNING_PURPOSE ) output = processOutputWithLearningPurpose(output,sirenLearningPurpose);
			queue.add(output);
		}};
		
	    alarm.siren.in.turnon = new Action() {public void action() {
			logger.debug("    siren_turnon");
			String output = "siren:turnon";
			if ( USE_LEARNING_PURPOSE ) output = processOutputWithLearningPurpose(output,sirenLearningPurpose);
			queue.add(output);
		}};
		
	}
	
	String processOutputWithLearningPurpose(String outputAboveComponent,LearningPurpose learningPurpose){
		// output from above component is input for the learning purpose of the interface
		String inputForLP= outputAboveComponent; 
		boolean enabled_input=learningPurpose.stepOnInput(inputForLP);  
		if (!enabled_input) {
			inputForLP=inputForLP + "=" + IF_ILLEGAL_INPUT;
		}
		return inputForLP;
	}
	
	@Override
	public void pre() {
		// add any code here that should be run at the beginning of every 'session',
		// i.e. put the system in its initial state

		// reset
		reset();

		logger.debug("Starting SUL");		
	}
	
	@Override
	public void post() {
		// add any code here that should be run at the end of every 'session'
    	logger.debug("Shutting down SUL");
	}
	

    // adapter for sending INPUTS to alarm component and process through learning purpose:
    //------------------------------------------------------------------------------
    //  - for interface above : output of interface
    //  - for interface below : input of interface 
	
	@Override
	public  String step( String input ) throws SULException {
		String output;
		logger.debug("  input: " +input);
		
		// alarm input
		switch (input) {
	    case "console:arm":
	    	alarm.console_arm();
            break;
	    case "console:disarm":
	    	alarm.console_disarm();
            break;
	    case "sensor:disabled":
			if ( USE_LEARNING_PURPOSE ) {
				// verify whether sensor output is possible according learning purpose
				boolean possible_interface_output=sensorLearningPurpose.stepOnOutput(input);
				if (!possible_interface_output)  {
					output=IF_IMPOSSIBLE_OUTPUT;
				//	if ( CONSOLE_OUTPUTS_ONLY ) output = INTERNAL_OUTPUT;
					return output;					
				} 
			}	
   		    // let alarm deal with possible sensor output
	    	alarm.sensor_disabled();
            break;
	    case "sensor:triggered":
			if ( USE_LEARNING_PURPOSE ) {
				// verify whether sensor output is possible according learning purpose
				boolean possible_interface_output=sensorLearningPurpose.stepOnOutput(input);
				if (!possible_interface_output)  {
					output=IF_IMPOSSIBLE_OUTPUT;
				//	if ( CONSOLE_OUTPUTS_ONLY ) output = INTERNAL_OUTPUT;
					return output;					
				} 
			}	
			// let alarm deal with possible sensor output
    	    alarm.sensor_triggered();    	   
            break;
	    default: 
            output="undefined";
            logger.debug("  out: " + output );
    		return output;
            //break;   
		}
		
		if ( CONSOLE_OUTPUTS_ONLY ) { 
	       output = getConsoleOutput();
	    } else {
	       output = getOutput();
	    }	
		logger.debug("  out: " + output );
		return output;
	}

	/**
	 *  only allow outputs from console
	 *  if there are none we use INTERNAL_OUTPUT to signify that the input caused some internal changes but no output.
	 * @return
	 */
	public  String getConsoleOutput() {
		String output = "";
		while ( ! queue.isEmpty() ) {
			String newoutput =queue.removeLast();
			if ( newoutput.startsWith("console:")) {
			   if ( ! output.equals("") ) {
				  output = output + ", ";
			   }			
			   output =  output + newoutput;
			}
		}
		if ( output.equals("") ) { 
			output=INTERNAL_OUTPUT;  // on input alarm system does not send an output to its top interface ( IConsole); it does internal actions: only sends outputs to lower requiring interfaces
		}
		return output;
	}
	
	public String getOutput() {
		String output = "";
		while ( ! queue.isEmpty() ) {
			if ( ! output.equals("") ) output = output + ", ";
			output =  output + queue.removeLast();
		}			
		return output;
	}

}
