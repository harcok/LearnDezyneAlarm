package sul.alarmsystem;
import java.util.LinkedList;

/** basic test interface to alarm component
 * 
 * @author Harco Kuppens
 *
 */
public class Test {
	
	
	public static LinkedList<String> queue = new LinkedList<String>();
	public static Alarm alarm;

	public static String getOutput() {
		String output = "";
		while ( ! queue.isEmpty() ) {
			//System.err.println("  out: " + queue.removeLast() );
			if ( ! output.equals("") ) output = output + ", ";
			output =  output + queue.removeLast();
		}
		//System.err.println("  getOutput: " + output );
		return output;
	}	
		
	public static void main(String[] args) 
	{
		 
		Locator locator = new Locator();
		//Action illegal_handler = new Action() {public void action() {throw new RuntimeException("illegal");}};
		Action illegal_handler = new Action() {public void action() {
			System.err.println("    console:illegal");
			Test.queue.add("console:illegal");
		}};
		
		Runtime runtime = new Runtime(illegal_handler);
		
		locator.set(runtime);

		//construct the system
	    alarm = new Alarm(locator,"alarm");
		
	    alarm.console.out.deactivated = new Action() {public void action() {
			System.err.println("    console:deactivated");
			Test.queue.add("console:deactivated");
		}}; 
		alarm.console.out.detected = new Action() {public void action() {
			System.err.println("    console:detected");
			Test.queue.add("console:detected");
		}}; 
		
		alarm.sensor.in.enable =  new Action() {public void action() {
			System.err.println("    sensor:enable");
			Test.queue.add("sensor:enable");
		}}; 
		
		alarm.sensor.in.disable =  new Action() {public void action() {
			System.err.println("    sensor:disable");
			Test.queue.add("sensor:disable");
		}};
		
	    alarm.siren.in.turnoff = new Action() {public void action() {
			System.err.println("    siren:turnoff");
			Test.queue.add("siren:turnoff");
		}};
		
	    alarm.siren.in.turnon = new Action() {public void action() {
			System.err.println("    siren:turnon");
			Test.queue.add("siren:turnon");
		}};
		
		//alarm.sensor.in.enable
		
		step("console:arm");
		step("console:disarm");
		step("sensor:disabled");
		step("console:arm");
		step("console:arm");
		step("sensor:triggered");
		step("sensor:disabled");
		step("console:disarm");
		step("sensor:disabled");

		
		

	}
	
	public static String step( String input ) {
		String output;
		System.err.println("");
		System.err.println(input);
		
		switch (input) {
	    case "console:arm":
	    	alarm.console_arm();
            break;
	    case "console:disarm":
	    	alarm.console_disarm();
            break;
	    case "sensor:disabled":
	    	alarm.sensor_disabled();
            break;
	    case "sensor:triggered":
	    	alarm.sensor_triggered();
            break;
	    default: 
            output="undefinedInput";
            break;   
		}
		
		
	    output = getOutput();
		System.err.println("  out: " + output );
		return output;
	}	
	
	

}
