package sul.alarmsystem;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.log4j.Logger;

/**
 
 @author Harco Kuppens
 

 ISirenLearningPurpose 
   For the learning it tracks which outputs from ISiren interface to alarm component are currently possible,
   so that the learner knows which of ISiren outputs it can send to the alarm component.  (changes per step in the learning query!)
   
   It does this by keeping the state of ISiren interface based on the inputs send to the ISiren interface by the alarm component.
   Then from the ISiren interface specification it is clear, for a specific state, which inputs are enabled, and which outputs can possible be emitted.   
   

 
// This is the interface to a siren used in the Alarm system.
// The Siren can be turned on and can be turned off. 
interface ISiren
{
  in void turnon();
  in void turnoff();

  behaviour
  {
    enum State { Off, On };
    State state = State.Off;

    [state.Off]
    {
      on turnon:        state = State.On;
      on turnoff:       illegal;
    }
    [state.On]
    {
      on turnoff:       state = State.Off;
      on turnon:        illegal;
    }
  }
}

 */

public class ISirenLearningPurpose implements LearningPurpose{
	private static final Logger logger = Logger.getLogger(ISensorLearningPurpose.class);
	enum State {
		    Off, On
	};
	
	State state;
    HashMap< State, HashSet<String> > state2enabledInputs = new HashMap<State, HashSet<String>>();
    HashMap< State, HashSet<String> > state2possibleOutputs = new HashMap<State, HashSet<String>>();
    
	public ISirenLearningPurpose() {
		state2enabledInputs.put(State.On,new HashSet<String> ( Arrays.asList("siren:turnoff") ) );
		state2enabledInputs.put(State.Off,new HashSet<String> ( Arrays.asList("siren:turnon") ) );

		state2possibleOutputs.put(State.On,new HashSet<String>() ); // empty
		state2possibleOutputs.put(State.Off,new HashSet<String>() ); //empty
		
		reset();
	}
	
	boolean isInputEnabled( String input ) {
		return state2enabledInputs.get(state).contains(input);
	}
	boolean isOutputPossible( String output ) {
		return state2possibleOutputs.get(state).contains(output);
	}
	
	
	/** reset to initial state 
	 */
	public void reset() {
		state=State.Off;			
	}
	
	/**
	 *  keeping track of current state by inputs from alarm component
	 * @param input
	 * 
	 * returns boolean saying whether input is enabled or not (illegal)
	 */
	public boolean stepOnInput ( String input ) {
		
		
		if ( !isInputEnabled(input) ) {
			logger.error("interface got illegal input '" + input + "' in state '" + state + "'");
			return false;
		}
		
		switch (state) {
	    case Off:
		    if ( input.equals("siren:turnon")) {	      
		        state = State.On;
		    }
		    break;
	    case On: 
			if ( input.equals("siren:turnoff")) {	      
			    state = State.Off;
			}
			break;		
		}
		return true;
	}	
	
	
	/**
	 *  keeping track of current state by outputs from ISiren interface caused by internal actions  
	 * @param output
	 * returns boolean saying whether output is possible or not
	 */
	public boolean stepOnOutput ( String output ) {
       // no outputs caused by internal actions
		return false;
	 }	

}
