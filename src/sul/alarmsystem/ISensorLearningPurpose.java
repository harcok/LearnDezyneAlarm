package sul.alarmsystem;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.log4j.Logger;

import learner.Main;



/**
@author Harco Kuppens
 
 ISensorLearningPurpose 
   For the learning it tracks which outputs from ISensor interface to alarm component are currently possible,
   so that the learner knows which of ISensor outputs it can send to the alarm component.  (changes per step in the learning query!)
   
   It does this by keeping the state of ISensor interface based on the inputs send to the ISensor interface by the alarm component.
   Then from the ISensor interface specification it is clear, for a specific state, which inputs are enabled, and which outputs can possible be emitted.   
   
 
 

 
// This is the interface to a (movement) sensor used in the Alarm system.
// The Sensor can be enabled and disabled, and the status of the Sensor after enabling
// is reported via the triggered and disabled events.
interface ISensor
{
  in void enable();
  in void disable();

  out void triggered();
  out void disabled();

  behaviour
  {
    enum State { Disabled, Enabled, Disabling, Triggered };
    State state = State.Disabled;

    [state.Disabled]
    {
      on enable:        state = State.Enabled;
      on disable:       illegal;
    }
    [state.Enabled]
    {
      on enable:        illegal;
      on disable:       state = State.Disabling;
      on optional:    { triggered; state = State.Triggered; }
    }
    [state.Disabling]
    {
      on enable,
         disable:       illegal;
      on inevitable:  { disabled; state = State.Disabled; }
    }
    [state.Triggered]
    {
      on enable:        illegal;
      on disable:       state = State.Disabling;
    }
  }
}

*/
public class ISensorLearningPurpose implements LearningPurpose {
	private static final Logger logger = Logger.getLogger(ISensorLearningPurpose.class);
	enum State {
		Disabled, Enabled, Disabling, Triggered   
    };

	State state;
	HashMap< State, HashSet<String> > state2enabledInputs = new HashMap<State, HashSet<String>>();
	HashMap< State, HashSet<String> > state2possibleOutputs = new HashMap<State, HashSet<String>>();
	
	public ISensorLearningPurpose() {
		state2enabledInputs.put(State.Disabled,new HashSet<String> ( Arrays.asList("sensor:enable") ) );
		state2enabledInputs.put(State.Enabled,new HashSet<String> ( Arrays.asList("sensor:disable") ) );
		state2enabledInputs.put(State.Disabling,new HashSet<String> ( ) ); // empty
		state2enabledInputs.put(State.Triggered,new HashSet<String> ( Arrays.asList("sensor:disable") ) );
		
		state2possibleOutputs.put(State.Disabled,new HashSet<String>() ); // empty
		state2possibleOutputs.put(State.Enabled,new HashSet<String>(Arrays.asList("sensor:triggered")) ); 
		state2possibleOutputs.put(State.Disabling,new HashSet<String>( Arrays.asList("sensor:disabled") ) ); 
		state2possibleOutputs.put(State.Triggered,new HashSet<String>() ); //empty
		
		reset();
	}
	
	boolean isInputEnabled( String input ) {
		return state2enabledInputs.get(state).contains(input);
	}
	boolean isOutputPossible( String output ) {
		return state2possibleOutputs.get(state).contains(output);
	}
	
	
	/** reset to initial state 
	 */
	public void reset() {
		state=State.Disabled;			
	}
	
	/**
	 *  keeping track of current state by inputs from alarm component
	 * @param input
	 * 
	 *  returns boolean saying whether input is enabled or not (illegal)
	 */
	public boolean stepOnInput ( String input ) {
		
		if ( !isInputEnabled(input) ) {
			// if above component sends input which is illegal for interface below it this is seen as bug in component
			logger.error("interface cannot step on illegal input '" + input + "' in state '" + state + "'");
			return false;
		}
		
		switch (state) {
	    case Disabled:
			if ( input.equals("sensor:enable")) {	      
		        state = State.Enabled;
		    }
			break;
	    case Enabled: 
			if ( input.equals("sensor:disable")) {	      
			    state = State.Disabling;
			}
			break;			
		case Triggered: 
			if ( input.equals("sensor:disable")) {	      
			    state = State.Disabling;
			}
			break;
		default:
			break;		
        }
		return true;
	 }
	
	/**
	 *  keeping track of current state by outputs from ISensor interface caused by internal actions  
	 * @param output
	 * returns boolean saying whether output is possible or not
	 */
	public boolean stepOnOutput ( String output ) {
		
		if ( !isOutputPossible(output) ) {
			// An interface can  generate an output without an input because of some internal behavior,
			// this output must be considered for learning an above component which can get this output as input. 
			// However it is of course alright if such output is not possible, we will discard it as input for above component.  
			logger.debug("interface cannot step on impossible output '" + output + "' in state '" + state + "'");
			return false;
		}
		
		switch (state) {
	    case Enabled: 
	    	// on optional:    { triggered; state = State.Triggered; }
			if ( output.equals("sensor:triggered")) {	      
			    state = State.Triggered;
			}
			break;			
		case Disabling: 
			// on inevitable:  { disabled; state = State.Disabled; }
			if ( output.equals("sensor:disabled")) {	      
			    state = State.Disabled;
			}
			break;
		default:
			break;		
        }
		
		return true;
	 }	
		

}
