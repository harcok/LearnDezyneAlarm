package sul.alarmsystem;


// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.
class Alarm extends Component {
  enum State {
    Disarmed, Armed, Disarming
  };

  Alarm.State state;
  Boolean sounding;

  IConsole console;
  ISensor sensor;
  ISiren siren;

  public Alarm(Locator locator) {this(locator, "");};

  public Alarm(Locator locator, String name) {this(locator, name, null);};

  public Alarm(Locator locator, String name, SystemComponent parent) {
    super(locator, name, parent);
    this.flushes = true;
    state = Alarm.State.Disarmed;
    sounding = false;
    console = new IConsole();
    console.in.name = "console";
    console.in.self = this;
    sensor = new ISensor();
    sensor.out.name = "sensor";
    sensor.out.self = this;

    siren = new ISiren();
    siren.out.name = "siren";
    siren.out.self = this;

    console.in.arm = () -> {Runtime.callIn(this, () -> {console_arm();}, new Meta(this.console, "arm"));};

    console.in.disarm = () -> {Runtime.callIn(this, () -> {console_disarm();}, new Meta(this.console, "disarm"));};

    sensor.out.triggered = () -> {Runtime.callOut(this, () -> {sensor_triggered();}, new Meta(this.sensor, "triggered"));};

    sensor.out.disabled = () -> {Runtime.callOut(this, () -> {sensor_disabled();}, new Meta(this.sensor, "disabled"));};

  }
  public void console_arm() {
    if (state == Alarm.State.Disarmed) {
      {
        sensor.in.enable.action();
        state = Alarm.State.Armed;
      }
    }
    else if (state == Alarm.State.Armed) {
      this.runtime.illegal.action();
    }
    else if (state == Alarm.State.Disarming) {
      this.runtime.illegal.action();
    }
    else this.runtime.illegal.action();
  };

  public void console_disarm() {
    if (state == Alarm.State.Disarmed) {
      this.runtime.illegal.action();
    }
    else if (state == Alarm.State.Armed) {
      {
        sensor.in.disable.action();
        state = Alarm.State.Disarming;
      }
    }
    else if (state == Alarm.State.Disarming) {
      this.runtime.illegal.action();
    }
    else this.runtime.illegal.action();
  };

  public void sensor_triggered() {
    if (state == Alarm.State.Disarmed) {
      this.runtime.illegal.action();
    }
    else if (state == Alarm.State.Armed) {
      {
        console.out.detected.action();
        siren.in.turnon.action();
        sounding = true;
      }
    }
    else if (state == Alarm.State.Disarming) {
      this.runtime.illegal.action();
    }
    else this.runtime.illegal.action();
  };

  public void sensor_disabled() {
    if (state == Alarm.State.Disarmed) {
      this.runtime.illegal.action();
    }
    else if (state == Alarm.State.Armed) {
      this.runtime.illegal.action();
    }
    else if (state == Alarm.State.Disarming && sounding) {
      {
        console.out.deactivated.action();
        
        //error when following line commented out: 
        siren.in.turnoff.action();
       
        sounding = false;
        state = Alarm.State.Disarmed;
      }
    }
    else if (state == Alarm.State.Disarming && ! (sounding)) {
      {
        console.out.deactivated.action();
        state = Alarm.State.Disarmed;
      }
    }
    else this.runtime.illegal.action();
  };

}
