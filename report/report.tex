\documentclass[]{article}
\usepackage{graphicx}
\usepackage{hyperref}

\usepackage{varwidth}
\usepackage{verbatim}

\newenvironment{centerverbatim}{%
	\par
	\centering
	\varwidth{\linewidth}%
	\verbatim
}{%
	\endverbatim
	\endvarwidth
	\par
}

%opening
\title{Learning Dezyne's Alarm System example}
\author{Harco  Kuppens}

\begin{document}

\maketitle



\section{Introduction}

The tool Dezyne from the company Verum does add automated verification
to component based software development. Automated formal verification
discovers hidden defects that are otherwise practically impossible
to find.

The Dezyne tool comes with a set of examples. In this document we
focus on the Alarm System example. The Alarm System example describes
a simple alarm system consisting of a console, a siren and a sensor.

\begin{figure}[h]
	\begin{centering}
		\includegraphics{intro/AlarmSystemDezyne}
		\par\end{centering}
	\caption{Alarm System}
	\label{fig:alarmsystem}
\end{figure}

The user can use the console to either enable or disable the alarm
system, causing the sensor also to be enabled or disabled. When the
alarm system is activated and the sensor is triggered then the siren
gets activated, otherwise the siren is silent. If the sensor is triggered
it cannot be triggered again. However if you disable the sensor and
enable it again then it can be triggered again.

When specifying a component hierarchy in Dezyne each component needs to write an interface specifying its behavior. The implementation of the component can also be implemented in Dezyne by specifying its behavior, but is may also be implemented externally as long it obeys its interface specification. 
In figure~\ref{fig:alarmsystem}  the Alarm component with its interface IConsole is shown. The Alarm component requires to function two other components which need to obey the interfaces ISensor and ISiren. The example contains only one component, the Alarm component, and it contains all interfaces the Alarm component needs to interact with other components. 

The behavior for each component and interface is specified in the Dezyne alarm model Alarm.dzn which you view in full in  appendix~\ref{app:Alarm.dzn}.  The  .dzn file format uses an easy text based language to specify the behavior for interfaces and components. For example the ISensor interface behavior is specified as:


\medskip

\label{verb:ISensor}
{\scriptsize 
\begin{centerverbatim}

interface ISensor
{
  in void enable();
  in void disable();

  out void triggered();
  out void disabled();

  behaviour
  {
    enum State { Disabled, Enabled, Disabling, Triggered };
    State state = State.Disabled;

    [state.Disabled]
    {
      on enable:        state = State.Enabled;
      on disable:       illegal;
    }
    [state.Enabled]
    {
      on enable:        illegal;
      on disable:       state = State.Disabling;
      on optional:    { triggered; state = State.Triggered; }
    }
    [state.Disabling]
    {
      on enable,
         disable:       illegal;
      on inevitable:  { disabled; state = State.Disabled; }
    }
    [state.Triggered]
    {
      on enable:        illegal;
      on disable:       state = State.Disabling;
    }
  }
}
\end{centerverbatim} 
}


\medskip


 The sensor's behavior is described by four states Disabled, Enabled,Disabling, and Triggered. Each state can receive events, however the states are not always input complete. For  example 
state.Disabled does not allowed the "disabled" input by specifying it illegal. The component using this interface should obey this specification, and to be correct it should never send an input "disabled" to the component implementing the ISensor interface when the ISensor interface is in state.Disabled.
 


There are also two special internal events which are  not normal input events but can be triggered at some time by internal behavior of the component:
\begin{description}
	\item[optional] Without needing an input this "optional" event action can be optionally be triggered in this state.
	\item[inevitable] Without needing an input this "inevitable" event action will, as long no other input event occurs,  eventually be triggered in this state.
\end{description}

These internal events cause nondeterministic asynchronous output actions from the component implementing the interface to the component requiring the interface. From the requiring component's view they are asynchronous input events. 


When you first start Dezyne and open the Alarm System example, then the Alarm component in Alarm.dzn will initially will not be verified correctly with respect to its interfaces. It will give the  sequence shown in figure~\ref{fig:errorsequence}  to demonstrate the error found by verification.


\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.5]{intro/sequence_dezyne_leading_to_illegal_action}
		\par\end{centering}
	\caption{Error sequence leading to an illegal  turnon event send to the Siren}
	\label{fig:errorsequence}
\end{figure}


The fix is done by adding a forgotten siren.turnoff() action in the Disarming state of the Alarm component when sounding is active. After this fix the model is verified correct, and we now can start learning it!

For more information about Dezyne see:
\begin{itemize}
	\item Dezyne Tutorial, url: \url{http://dezyne.verum.com/exampleslist?dezyne-tutorial}
	\item Dezyne online help, url: \url{http://dezyne.verum.com/docs/0.1.0/nav/0}
\end{itemize}

\newpage


\section{Learning Alarm component input enabled}

For learning with the LearnLib tool it is required that the System Under Learning (SUL) is input enabled. If we look at the definition of the alarm component in Alarm.dzn it is not input enabled because some inputs are considered illegal. However we can apply a trick to make it input enabled, by when an illegal exception occurs we translate it to an "illegal" output. 

So for the alarm component we have the following : 
\begin{description}
	\item[inputs:] console:arm, console:disarm, sensor:disabled, sensor:triggered
	\item[outputs:] console:detected, console:deactivated, console:illegal, sensor:enable, sensor:disable, siren:turnon, siren:turnoff
\end{description}

In the first part of an event we define the origin of the event, and in the second part the name of the event. Notice that there is only a console:illegal event and not siren:illegal nor a sensor:illegal event. This is because during learning of the alarm component we choose to use only the real outputs of the interfaces used as input for  the alarm component, so we won't use a siren:illegal nor a sensor:illegal as input for learning. However because we disregard any interface specification during learning the alarm system itself can get an input which it considers illegal,
so we can have an console:illegal output.

Some of the input events can be send asynchronously from the alarm component's required interfaces. However when doing active learning we are in control of sending the inputs. We are not interested how they did originated from external components. We are only interested how they effect the component we are learning. The execution semantics of a Dezyne component is run to completion before any other input event is allowed to cause action. This means that timing of the input actions doesn't matter, only their order does matter. So the learner is free to choose the timing of sending the inputs, only the order effects the behavior.

So to learn the system I used Dezyne to generate java8 source code for the Alarm component and using the Dezyne runtime.java for java8  I wrote an adapter interface to connect the alarm component to the learning tool.  Basically we wrapped the learning component in a AlarmSul class implementing de.learnlib.api.SUL java interface which is required for a SUL to be learned with LearnLib The result of learning is shown in figure~\ref{fig:model_input_enabled}

\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.3]{Learning_Alarm_component_input_enabled/learnedModel.pdf}
		\par\end{centering}
	\caption{Learned model of Alarm component input enabled}
	\label{fig:model_input_enabled}
\end{figure}


When a input is send to the alarm component it can do multiple actions. For example when the sensor sends a sensor:disabled event to the alarm component it does two actions: it turns the siren of  (siren:turnof) and informs the console that the alarm is deactivated (console:deactivated). 
However because the execution semantics of a Dezyne component is run to completion before any other input event is allowed to cause action, we can collect all caused output actions during learning before we send a new input.
Therefore notice that an output of the alarm component can consist of several outputs as we can seen in  figure~\ref{fig:model_input_enabled} 
for input sensor:disable in state s3 which has as output "siren:turnoff, console:deactivated".

We can manually compare the learned model with the behavior of the alarm component in the Alarm.dzn model, shown below\ref{verb:Alarm}, that they have similar input output behavior. In the Dezyne model an internal boolean variable $sounding$ is used to distinguish between a state where the sensor is triggered and the alarm is sounding and a state where the sensor isn't triggered and the alarm is not sounding. However in the learned model this internal boolean variable is encoded in the different states s2 and s4. Note that we could also automatically verified this similarity, however manually was quicker for this case.

\medskip

\label{verb:Alarm}
{\scriptsize 
\begin{centerverbatim}

component Alarm
{
    provides IConsole console;
    requires ISensor sensor;
    requires ISiren siren;

  behaviour
  {
    enum State { Disarmed, Armed, Disarming };
    State state = State.Disarmed;
    bool sounding = false;

    [state.Disarmed]
    {
      on console.arm():         { sensor.enable(); state = State.Armed; }
      on console.disarm(),
         sensor.triggered(),
         sensor.disabled():       illegal;
    }
    [state.Armed]
    {
      on console.arm():           illegal;
      on console.disarm():      { sensor.disable(); state = State.Disarming; }
      on sensor.triggered():    { console.detected();
                                  siren.turnon();
                                  sounding = true;
                                }
      on sensor.disabled():       illegal;
    }
    [state.Disarming]
    {
      on console.arm(),
         console.disarm():        illegal;
      on sensor.triggered():      illegal;
      on sensor.disabled():
          { [sounding]          { console.deactivated();
                                  siren.turnoff();                                  
                                  sounding = false;
                                  state = State.Disarmed;
                                }
            [otherwise]         { console.deactivated();
                                  state = State.Disarmed;
                                }
          }
    }
  }
}

\end{centerverbatim} 
}


\medskip


\section{Deriving Alarm component interface from learned model }

The interface IConsole of the Alarm component only sees the outputs  send to it by the alarm component: console:detected, console:deactivated, and console:illegal. But it doesn't see the outputs 
sensor:enable, sensor:disable, siren:turnon and siren:turnoff.  From the IConsole point of view these are internal actions which are hidden from its view. Thus IConsole has a limited view on the model of the Alarm component. Therefore it should be possible to derive the IConsole interface model from the alarm component model by hiding the actions in the model which are for IConsole hidden. We do this by 
\begin{itemize}
	\item replacing a hidden inputs by a INTERN input, 
	\item replacing one or several hidden outputs by a INTERN output
	\item omit a hidden output which comes together with a visible output 
	\item omit a self loop transition with input INTERN and output  console:illegal 
\end{itemize}
When applying this transformation on the learned model in figure~\ref{fig:model_input_enabled} we get the interface  model in figure~\ref{fig:interface_input_enabled} . Note that we applied the transformation manual in the learned model's graphviz dot file,  however this can easily be automated.


\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.5]{Learning_Alarm_component_input_enabled/DerivedInterface.pdf}
		\par\end{centering}
	\caption{Derived interface of learned model of Alarm component input enabled}
	\label{fig:interface_input_enabled}
\end{figure}  
   
The Dezyne tool also has a statechart view on components and interfaces. The statechart view Dezyne gives for IConsole is shown in figure~\ref{fig:IConsoleInterface}. 

\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.4]{Learning_Alarm_component_input_enabled/IConsoleInterface.png}
		\par\end{centering}
	\caption{IConsole Interface}
	\label{fig:IConsoleInterface}
\end{figure}  

If we look at the derived interface  model in figure~\ref{fig:interface_input_enabled}  we can see that state s3 and s4 can be merged leaving a 4 state model.  

Comparing that with the interface model in figure~\ref{fig:IConsoleInterface} it is almost similar.  First three differences we can remove easily:

\begin{enumerate}
	\item  First the derived interface model shows INTERN and illegal actions, but the interface model just hides them. We can just also hide them in the derived interface model. 
 
 	\item  Second  the interface model has "on  optional: detected" label where the derived interface model  has just "INTERN/detected" without optional. However if we would look at original learned model we see that the original INTERN event for this transition is "sensor:disabled", and this event is "optional" in the ISensor interface, making the output detected of the IConsole also "optional". A similar reasoning can be applied that the output deactivated which can be made "inevitable".
 	
\end{enumerate}

\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.5]{Learning_Alarm_component_input_enabled/DerivedInterface2.pdf}
		\par\end{centering}
	\caption{Improved derived interface of learned model of Alarm component input enabled}
	\label{fig:interface_input_enabled2}
\end{figure}  

When applying the merging of s3 and s4 and the above  two updates we get an improved  derived interface model shown in figure~\ref{fig:interface_input_enabled2}. If we look at that model we can see the only difference we cannot remove: the dervide interface model has a self-loop in state s2 with output detected  but  IConsole doesn't has this self-loop in the matching state state.Triggered.  Now we ask our self where is this difference coming from?  The answer is in the introduction where we said : "If the sensor is triggered
it cannot be triggered again". This is a limitation introduced by the ISensor interface however during learning we always allowed any input to learn an input enabled model. In state s2 in the learned model we allow the sensor:triggered input for the second time, however the ISensor interface doesn't allow this, and therefor it shouldn't be there as in the IConsole interface.

Although the alarm component in the alarm example is input enabled its intended usage with surrounded components limits certain inputs to it. So we should have instead used in the interfaces during learning to disable certain inputs at certain times as specified by the interfaces. This disabling/enabling of inputs during inputs we can do by using a learning purpose which we going to investigate in the next section.

 
 
\newpage

\section{Learning Alarm component using learning purpose}

To let the learner obey the interfaces I implemented for each interface the alarm component requires, ISensor and ISiren, a learning purpose. During learning we send inputs to the alarm component, the SUL we are learning, and the alarm component responds with outputs. For both the inputs and outputs the learning purpose is used to guide the learner in learning:

\begin{description}
	\item[input from learner] The learning purpose checks  for an input from the learner whether the interface from where the input originates is able to send the input. Sometimes the interface is in a state where it cannot send this input as output. In that case the learning purpose  decides not to send the input to the  SUL because it is disallowed by the interface. Instead the learning purpose directly returns the output "DISABLED\_INPUT" to the learner. In this way the SUL is not affected and the learner will learn this input as self-loop in the model which we after learning can easily remove.
	
	\item[output from SUL]  If the output from the SUL which is send to a interface the learning purpose checks whether this interface is in a state where it allows this input. In the case it is in a state where it finds the input illegal the learning purpose will take this output and appends the predicate "=ILLEGAL\_OUTPUT" to the output before sending it as an input to the learner. Notice that if the SUL component is correct this shouldn't happen, however if it isn't we should see this incorrect behavior appearing in our learned model.
\end{description}

For the learning purpose to work it must now the state each interface is in. Therefore the learning purpose implements a simple statemachine for each interface which its updates for each input from the learner and each output from the SUL. The latter is needed because state changes in an interface can change from a internal "optional" or "inevitable" event and is informed to user of the interface by its output. Notice that the client of the interface always must be informed of the current state of the interface because otherwise it wouldn't be able to obey its specification. Eg. is impossible to know when an input is illegal if the client doesn't know in which state the interface is.

Learning with the learning purpose results in the learned model shown in figure~\ref{fig:model_learning_purpose}. 

\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.2]{Learning_Alarm_component_learning_purpose/learnedModel.pdf}
		\par\end{centering}
	\caption{Learned model of Alarm component using learning purpose}
	\label{fig:model_learning_purpose}
\end{figure}

When we look at the learned model using the learning purpose we can immediately see that the self-loop in state s2 is now changed from "sensor:triggered / siren:turnon, console:detected" into "sensor:triggered / DISABLED\_INPUT". So the learning purpose made sure we cannot trigger the sensor twice!

\newpage

\section{Deriving Alarm component interface from learned model using learning purpose }


When we derive an interface model from the learned model using the learning purpose  we get the derived interface model shown in figure~\ref{fig:interface_learning_purpose}. Note that we also removed the self-loops with output DISABLED\_INPUT because they represent transitions which are not allowed by the interfaces, and wouldn't normally not happen, but they happen during learning because the learner requires an input complete alphabet. When we look at derived interface model we can see that it is equivalent to the IConsole Interface model, so by introducing the learner purpose we managed to automatically learn the interface for the alarm component.

\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.4]{Learning_Alarm_component_learning_purpose/DerivedInterface.pdf}
		\par\end{centering}
	\caption{Derived interface of learned model of Alarm component using learning purpose}
	\label{fig:interface_learning_purpose}
\end{figure}

\newpage

\section{Learning Alarm interface directly  using learning purpose}


Instead of deriving the Alarm interface from the alarm model we could also learn it directly.
The learner learns the model of the input output behavior it sees, so if we only allow the learner to see the outputs which go to its own interface but hide all outputs to requiring interfaces it should learn the model from the viewpoint of its interface.

In the learner adapter to the SUL we can easily remove all outputs to its requiring interfaces. Because the learner always requires an output for an input we give back the output INTERNAL if there are is no output left after the remove step. The INTERNAL output  signifies  that the input caused some internal changes but no output. The model we learn when using this output mangling is shown in figure~\ref{fig:interface_model_direct}.

\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.3]{Learning_Alarm_interface_directly_using_learning_purpose/learnedModel.pdf}
		\par\end{centering}
	\caption{Derived interface of learned model of Alarm component using learning purpose}
	\label{fig:interface_model_direct}
\end{figure}

If we simply strip all transitions with output "console:illegal" and "DISABLED\_INPUT" we get the interface model in figure~\ref{fig:interface_model_direct_stripped}.

\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.4]{Learning_Alarm_interface_directly_using_learning_purpose/learnedModelStripped.pdf}
		\par\end{centering}
	\caption{Derived interface of learned model of Alarm component using learning purpose}
	\label{fig:interface_model_direct_stripped}
\end{figure}

If we also replace "sensor:triggered" by OPTIONAL and "sensor:disabled" by "INEVITABLE" model  figure~\ref{fig:interface_model_direct_stripped} is exactly the same as the earlier derived IConsole interface in figure~\ref{fig:interface_learning_purpose}. 

We directly learned the interface model of the alarm component. This direct method is simpler because for example  we didn't need to do the state merging which was needed when we derived the interface model from the learned model. However directly learning  would not be possible with LearnLib if the interface model was non-deterministic. In that case we should use a learner tool which can learn non-deterministic models. But as long the component itself is deterministic we could derive the interface model even though the  interface model itself is non-deterministic.


\newpage 


\section{Learning Alarm component with bug}

If we now take the original alarm example model with the bug described in the introduction would we be able to find it in the learned model?

In the generated source code of the alarm component I reintroduced the bug by commenting out
the siren.turnoff() action in the Disarming state of the Alarm component when sounding is active.
Then I learned the model again using the learning purpose which is shown in figure~\ref{fig:learning_buggy_model}.

\begin{figure}[h!]
	\begin{centering}
		\includegraphics[scale=0.2]{Learning_Buggy_Alarm_Model_using_learning_purpose/learnedModel.pdf}
		\par\end{centering}
	\caption{Learned model of buggy Alarm component using learning purpose}
	\label{fig:learning_buggy_model}
\end{figure}

If we look at this model we can see that there is an "ILLEGAL\_OUTPUT" output  in the model in the transition from state s6 to state s2. You can reach this output by the trace: 
\medskip

\label{verb:ISensor}
{\scriptsize 
	\begin{centerverbatim}
console:arm / sensor:enable  
sensor:triggered / siren:turnon,console:detected
console:disarm / sensor:disable 
sensor:disabled / console:deactivated
console:arm / sensor:enable
sensor:triggered / siren:turnon=ILLEGAL_OUTPUT, console:detected
	\end{centerverbatim} 
}
\medskip

This is exactly the same trace give by the  sequence shown in figure~\ref{fig:errorsequence}  derived by the Dezyne tool. So we found  the same trace leading to a bug in the Alarm component leading to an illegal action. 



\section{Final thoughts}

In this report I investigated several ways how learning can be used with the components and interfaces of a Dezyne model. We were pretty successful, however the component we learned was a pretty simple model. For more complex models learning becomes more complicated and it is never sure if you learned the right model.  Still a learned model which is not the correct model can still be helpful, eg. finding illegal action bug in component model. 

Also I tried to learn automatically an interface for a component. However this is only possible if the model of the interface is deterministic. Directly learning  would not be possible with the LearnLib tool if the interface model was non-deterministic. In that case we should use a learner tool which can learn non-deterministic models. But as long the component itself is deterministic we could derive the interface model even though the  interface model itself is non-deterministic.

More research is needed to see how above methods work for bigger models, and how we can learn non-deterministic interface models.



\newpage 

\appendix

\section{Dezyne model : Alarm.dzn}
\label{app:Alarm.dzn}
{\scriptsize 


\begin{verbatim}
//purpose: Show illegal behaviour in component

/////////////////////////////////////////////////////////////////////////////
// This example shows how an illegal behaviour is revealed when verifying a
// Dezyne component and (in a commented out section) provides guidelines on how
// to ensure that the reported problem does not occur anymore.
///////////////////////////////////////////////////////////////////////////////



// This is the interface between a Console and an Alarm system.
// The Alarm can be armed and disarmed, and the status of the system after arming
// is reported via the detected and deactivated events.
interface IConsole
{
    in void arm();
    in void disarm();

    out void detected();
    out void deactivated();

  behaviour
  {
    enum State { Disarmed, Armed, Triggered, Disarming };
    State state = State.Disarmed;

    [state.Disarmed]
    {
      on arm:           state = State.Armed;
      on disarm:        illegal;
    }
    [state.Armed]
    {
      on disarm:        state = State.Disarming;
      on optional:    { detected; state = State.Triggered; }
      on arm:           illegal;
    }
    [state.Triggered]
    {
      on disarm:        state = State.Disarming;
      on arm:           illegal;
    }
    [state.Disarming]
    {
      on inevitable:  { deactivated; state = State.Disarmed; }
      on arm,
         disarm:        illegal;
    }
  }
}

// This is the interface to a (movement) sensor used in the Alarm system.
// The Sensor can be enabled and disabled, and the status of the Sensor after enabling
// is reported via the triggered and disabled events.
interface ISensor
{
  in void enable();
  in void disable();

  out void triggered();
  out void disabled();

  behaviour
  {
    enum State { Disabled, Enabled, Disabling, Triggered };
    State state = State.Disabled;

    [state.Disabled]
    {
      on enable:        state = State.Enabled;
      on disable:       illegal;
    }
    [state.Enabled]
    {
      on enable:        illegal;
      on disable:       state = State.Disabling;
      on optional:    { triggered; state = State.Triggered; }
    }
    [state.Disabling]
    {
      on enable,
         disable:       illegal;
      on inevitable:  { disabled; state = State.Disabled; }
    }
    [state.Triggered]
    {
      on enable:        illegal;
      on disable:       state = State.Disabling;
    }
  }
}

// This is the interface to a siren used in the Alarm system.
// The Siren can be turned on and can be turned off.
interface ISiren
{
  in void turnon();
  in void turnoff();

  behaviour
  {
    enum State { Off, On };
    State state = State.Off;

    [state.Off]
    {
      on turnon:        state = State.On;
      on turnoff:       illegal;
    }
    [state.On]
    {
      on turnoff:       state = State.Off;
      on turnon:        illegal;
    }
  }
}



// Component specification for an Alarm system which implements a Console, and
// uses a (movement) Sensor and a Siren
component Alarm
{
    provides IConsole console;
    requires ISensor sensor;
    requires ISiren siren;

  behaviour
  {
    enum State { Disarmed, Armed, Disarming };
    State state = State.Disarmed;
    bool sounding = false;

    [state.Disarmed]
    {
      on console.arm():         { sensor.enable(); state = State.Armed; }
      on console.disarm(),
         sensor.triggered(),
         sensor.disabled():       illegal;
    }
    [state.Armed]
    {
      on console.arm():           illegal;
      on console.disarm():      { sensor.disable(); state = State.Disarming; }
      on sensor.triggered():    { console.detected();
                                  siren.turnon();
                                  sounding = true;
                                }
      on sensor.disabled():       illegal;
    }
    [state.Disarming]
    {
      on console.arm(),
         console.disarm():        illegal;
      on sensor.triggered():      illegal;
      on sensor.disabled():
          { [sounding]          { console.deactivated();
          	
  								  /*
									If the next line is left commented out then an illegal behaviour is spotted;
									the Alarm makes an attempt to turn on the Siren when that is already turned on.
									(run Verify and check the Sequence Diagram).
									
									The problem can easily be solved by turning off the Siren when Disarming the Alarm.
									Undo the commenting, press Update and re-run verification.
								  */
                                  siren.turnoff();
                                  
                                  sounding = false;
                                  state = State.Disarmed;
                                }
            [otherwise]         { console.deactivated();
                                  state = State.Disarmed;
                                }
          }
    }
  }
}

\end{verbatim}
}

\end{document}
